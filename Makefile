#REPO?=kubespheredev/ks-installer
#TAG:=$(shell git rev-parse --abbrev-ref HEAD | sed -e 's/\//-/g')-dev
REPO=registry.gitlab.com/sonaproject/ks-installer
TAG=v3.3.2

build:
	docker build . --file Dockerfile --tag $(REPO):$(TAG)
push:
	docker push $(REPO):$(TAG)

cross-all:
	./multiarch_build.sh

all: build push
